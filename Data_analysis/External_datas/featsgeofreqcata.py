# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 13:14:38 2021

@author: edoua
"""

import pandas as pd
import numpy as np

df=pd.read_csv('featsInseeModif', sep=';',index_col=0)
peril=pd.read_csv('perilsUtile.csv',sep=';',index_col=0)
freq_catastrophe=pd.read_csv('freq_final.csv',sep=';')

print(df.info())

#fusion du fichier df avec freq_final
data=df.join(freq_catastrophe.set_index('Insee'), on='Insee')
print(data.info())

#fusion des fichiers
df=data.join(peril.set_index('Identifiant'), on='Identifiant')

#vérification dimension et valeur manquante
print(df.shape)
print(df.isna().sum())

#changement des valeurs objets en numérique Inondations et coulées de boue':1,'Mouvements de terrain':2,
#'Mouvements de terrain différentiels consécutifs à la sécheresse et à la réhydratation des sols':3,
#"Inondations et chocs mécaniques liés à l'action des vagues":4
df['Périls']=df['Périls'].replace(to_replace={'Inondations et coulées de boue':1,'Mouvements de terrain':2,
                                              'Mouvements de terrain différentiels consécutifs à la sécheresse et à la réhydratation des sols':3,
                                              "Inondations et chocs mécaniques liés à l'action des vagues":4})

#remplace les nan qui sous entends qu'il n'y a pas de catastrophe par 0
df['Périls']=df['Périls'].fillna(0)

#Vérification de valeur manquante et du jeu de donnée
print(df.info())
pd.set_option('display.max_column', 40)
print(df.head())

#Stockage du fichier
df.to_csv('claimPérils2b.csv', sep=';')