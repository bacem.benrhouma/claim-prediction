# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 12:21:17 2021

@author: edoua
"""

import pandas as pd
import numpy as np

def prepo(df):
    #Changement de nom de la colonne Code INSEE et commune en vue de la fusion
    df=df.rename(columns = {'Code INSEE': 'Insee'})
    
    #suppréssion de la colonne ville pour ne pas submerger le fichier
    df=df.drop(['Commune'],axis=1)
    
    #renvoie du df
    return df

def fusion(df,df1):
    #fusion des dataframes plus renvoie
    df2=df.join(df1.set_index('Insee'), on='Insee')
    return df2   

#chargement des data externes Géo frequence
sin=pd.read_csv('frequence_inon.xls - FreqMoy_Inon_9516.csv',sep=',')
print(sin.info())

mouv=pd.read_csv('frequence_mouvement.csv',sep=';')
print(mouv.info())

sech=pd.read_csv('freq_sech.csv',sep=';')
print(sech.info())

seisme=pd.read_csv('freq_seisme.csv',sep=';')
print(seisme.info())

#on supprime la colonne Commune et change le nom de la colonne Code INSEE en Insee
sin=prepo(sin)
mouv=prepo(mouv)
sech=prepo(sech)
seisme=prepo(seisme)

#On change les modalitées pour les colonnes fréquences par des valeurs numériques 
#'Pas de sinistre ou de risque répertoriés à CCR':0,'Entre 0 et 1 ‰':1,'Entre 1 et 2 ‰':2,
#Entre 2 et 5 ‰':5,'Entre 5 et 10 ‰':10 et 'Plus de 10 ‰':20

#Pour le fichier Inondation
sin['Frequence']=sin['Frequence'].replace(to_replace={'Pas de sinistre ou de risque répertoriés à CCR':0,'Entre 0 et 1 ‰':1,
                                                      'Entre 1 et 2 ‰':2,'Entre 2 et 5 ‰':5,'Entre 5 et 10 ‰':10,
                                                      'Plus de 10 ‰':20})
print(sin.head())

#Pour le fichier mouvement
mouv['Fréquence moyenne de sinistres\nmouvement de terrain']=mouv['Fréquence moyenne de sinistres\nmouvement de terrain'].replace(to_replace={'Pas de sinistre ou de risque répertoriés à CCR':0,
                                                                                    'Entre 0 et 1 ‰':1,'Entre 1 et 2 ‰':2,
                                                                                    'Entre 2 et 5 ‰':5,'Entre 5 et 10 ‰':10,
                                                                                    'Plus de 10 ‰':20})
print(mouv.head())

#pour le fichier secheresse
sech['Fréquence moyenne de sinistres\nsécheresse']=sech['Fréquence moyenne de sinistres\nsécheresse'].replace(to_replace={'Pas de sinistre ou de risque répertoriés à CCR':0,
                                                                                    'Entre 0 et 1 ‰':1,'Entre 1 et 2 ‰':2,
                                                                                    'Entre 2 et 5 ‰':5,'Entre 5 et 10 ‰':10,
                                                                                    'Plus de 10 ‰':20})
print(sech.head())

#pour le fichier seisme
seisme['Fréquence moyenne de sinistres\nséisme']=seisme['Fréquence moyenne de sinistres\nséisme'].replace(to_replace={'Pas de sinistre ou de risque répertoriés à CCR':0,
                                                                                    'Entre 0 et 1 ‰':1,'Entre 1 et 2 ‰':2,
                                                                                    'Entre 2 et 5 ‰':5,'Entre 5 et 10 ‰':10,
                                                                                    'Plus de 10 ‰':20})
print(seisme.head())

#fusion des datas un à un
freq_inondMouv=fusion(sin,mouv)
freq_inondMouvSech=fusion(freq_inondMouv,sech)
freq_final=fusion(freq_inondMouvSech,seisme)

print(freq_final.head())
print(freq_final.info())

#stockage du nouveau fichier
freq_final.to_csv('freq_final.csv', sep=';')