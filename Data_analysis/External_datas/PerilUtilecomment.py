# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 13:11:35 2021

@author: edoua
"""

import pandas as pd
import numpy as np

#chargement du fichier catastrophe naturelles et conversion en csv
climat = pd.read_excel('Arretes_de_catastrophe_naturelles.xlsx',index_col=None)
climat.to_csv('csvfile.csv', encoding='utf-8')

#extraction de l'année, pour pouvoir cibler les catastrophes à partir de 2012
climat['année_catastrophe']=climat['Date début'].apply(lambda r:r.year).astype(int)

#stockage des catastrophes à partir de 2012
climat=climat[climat['année_catastrophe']>2011]

#Changement du nom de la colonne Insee dans climat en vue du merge
climat=climat.rename(columns = {'INSEE': 'Insee'})
print(climat.info())

#import claim_c pour extraire les colonnes Insee et Identifiant pour les futures merges
df=pd.read_csv('claim_c.csv',sep=';',index_col=0)

#stockage des colonnes Insee et Identifiant
dfb=df[['Identifiant','Insee']]

data=pd.read_csv('feats.csv',sep=';',index_col=0)

#concaténation de data et dfb
data=pd.concat([data,dfb],axis=1)

#stockage du fichier avec feats Insee et Identifiant
data.to_csv('feats.csv', sep=';')

#importation de la variable cible en vue de la création d'un sous fichier contenant les catastrophes entrainants un sinistre
target=pd.read_csv('target.csv',sep=';',index_col=0)

"""
Lors du premier merge entre le fichier claim et géo_localisation ou geo_frequence
on a pu remarquer grâce à la fonction .join que de nombreux Insee du fichier claim
ne se mergeaient pas, donc on les a stocker grâce à :
'valeur_manqute=df[df['Insee].isna()]
Suivie d'une recherche de Insee sur http://geneatouque.free.fr/infodept/codcom/menu-codcom.html
Pour choisir soit la commune la plus proche ( dans le cas des Insee comportant
                                             un 9 ou la commune elle même,
                                             quand elle avait un autre numéro)
"""

#changement des insee comportant ..9.. car pas de correspondance dans le fichier localisation
data['Insee']=data['Insee'].replace(to_replace={'69382':'69381','62917':'62261','73920':'73227','38931':'38253',
                                                '83920':'83118','83924':'83069','59355':'59128','83923':'83107',
                                                '83923':'83107','57907':'57206','68908':'68297','06920':'06120',
                                                '13900':'13104','67948':'67180','73927':'73257','13904':'13001',
                                                '30937':'30133','06906':'06094','11910':'11262','33933':'33236',
                                                '38930':'38120','77900':'77407','13957':'13002','74924':'74208',
                                                '38928':'38191','62916':'62201','44926':'44154','13986':'13208',
                                                '15900':'15101','2A903':'2A228','83925':'83061','83933':'83070',
                                                '40901':'40128','38905':'38422','83900':'83068','06903':'06079',
                                                '34902':'34003','06901':'06120'})

"""
On a du changer les insee de 3 grandes ville, car pour le fichier 
Géo_frequence il n'y avait pas de disctintion entre les arrondissements
"""
#remplacement de certaine valeur pour faciliter la fusion avec location
data['Insee']=data['Insee'].replace(to_replace={'75102':'75056','75103':'75056','75104':'75056','75105':'75056',
                                                '75106':'75056','75107':'75056','75108':'75056','75109':'75056',
                                                '75110':'75056','75111':'75056','75112':'75056','75113':'75056',
                                                '75114':'75056','75115':'75056','75116':'75056','75117':'75056',
                                                '75118':'75056','75119':'75056','75120':'75056','75101':'75056'})
data['Insee']=data['Insee'].replace(to_replace={'13202':'13055','13203':'13055','13204':'13055','13205':'13055','13206':'13055',
                                                '13207':'13055','13208':'13055','13209':'13055','13210':'13055','13211':'13055',
                                                '13212':'13055','13213':'13055','13214':'13055','13215':'13055','13216':'13055',
                                                '13201':'13055'})
data['Insee']=data['Insee'].replace(to_replace={'69382':'69123','69383':'69123','69384':'69123','69385':'69123','69386':'69123',
                                                '69387':'69123','69388':'69123','69389':'69123','74270':'74123','69381':'69123'})

#Stockage du fichier data avec les Insee modifié
data.to_csv('featsInseeModif', sep=';')

#concaténation du jeu de donnée avec la target
data=pd.concat([data,target],axis=1)
print(data.info())

#création d'une colonne Code Département en extractant les 2 premiers caractères de la colonne Insee
data['Code Département']=data['Insee'].str[:2]

#stockage en vue de garder les insee Corse, car ils se mergent correctement et les insee non corse en vue de changer de type
corse=climat[(climat['Departement']=='2A') | (climat['Departement']=='2B')]
pas_corse=climat[(climat['Departement']!='2A') & (climat['Departement']!='2B')]

#stockage des insee Corse et non corse dans claim_loc
data_corse=data[(data['Code Département']=='2A') | (data['Code Département']=='2B')]
data_pas_corse=data[(data['Code Département']!='2A') & (data['Code Département']!='2B')]

#changement de type de la colonne Insee en int en vue de faciliter le merge
data_pas_corse['Insee']=data_pas_corse['Insee'].astype(int)
pas_corse['Insee']=pas_corse['Insee'].astype(int)

#suppréssion de colonne en vue du merge
corse=corse.drop(['Commune','Departement'],axis=1)
pas_corse=pas_corse.drop(['Commune','Departement'],axis=1)

#merge de la partie corse et non corse
df_corse=data_corse.join(corse.set_index('Insee'), on='Insee')
df_pas_corse=data_pas_corse.join(pas_corse.set_index('Insee'), on='Insee')

#concaténation horizontale 
data=pd.concat([df_corse,df_pas_corse],axis=0)
print(data.info())

#stockage des lignes ou la target=1 avec la condition que ft_2_categ=année_catastrophe en vue de créer un sous dossier
#pour avoir que les catastrophes qui présentent un intérêt 
data_cata=data[(data['target']==1) & (data['année_catastrophe']==data['ft_2_categ'])]

print(data_cata.info())

#vérification des doublons en vue de les supprimer
print(data_cata['Identifiant'].duplicated().value_counts())

#suppréssion des doublons
data_cata=data_cata.drop_duplicates(subset=['Identifiant'])

#stockage des colonnes utilent en vue de la fusion
data_cata_utile=data_cata[['Identifiant','Périls']]

#stockage du nouveau fichier
data_cata_utile.to_csv('perilsUtile.csv', sep=';')
