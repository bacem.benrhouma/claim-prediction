#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 11:06:28 2021

@author: M.Benrhouma
"""
#import des librairies
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.formula.api import ols

df = pd.read_csv('claim_c.csv',sep=';',index_col='Identifiant')
print(df.head())

#suppression de 'Unnamed: 0'
df.drop('Unnamed: 0',axis=1,inplace=True)

#selection des variables numériques
df_num=df.select_dtypes(include=['float'])

# calcul de corrélation entre les variables numériques
corr=df_num.corr()

#graphique de corrélation
fig, ax = plt.subplots(figsize=(5,5))
sns.heatmap(corr, annot= True, ax= ax, cmap="viridis",center=0)
plt.show();

#selection des variables catégorielles
df_cat=df.select_dtypes(exclude=['float'])

#suppression de 'Insee'
df_cat.drop('Insee',axis=1,inplace=True)

print(df_cat)

#Test de corrélation entre les variables catégorielles

#Test de corrélation via' V de cramer' entre les variables catégorielles

import association_metrics as am
# Convertir  les colonnes qui ont des str en  Category 
df_cat = df_cat.apply(
        lambda x: x.astype("category") if( x.dtype == "O") | (x.dtype == "int" ) else x)

# Initializer les objets de  CamresV en utilisant df_cramer
cramersv = am.CramersV(df_cat) 

#retournera une matrice par paires remplie du V de Cramer, où les colonnes et l’index sont les variables 
#catégorielles de df_cat
corr_cramer=cramersv.fit()

#graphique de corrélation
fig, ax = plt.subplots(figsize=(15,15))
sns.heatmap(corr_cramer, annot= True, ax= ax, cmap="viridis")
ax.set_title("Test de corrélation via' V de cramer' entre les variables catégorielles");
plt.show();
#on remarque une forte corrélation de l'ordre de 1 entre ft_15 et ft_16 avec les variables suivantes:
#de ft_6 jusqu'à ft_17 et ft_24 également ce qui nous laisse poser la question par rapport à ces deux variables:
#d'où vient la décision de les remplacer par ft_24 car elle n'a pas une corrélation égal à 1 avec le reste des variables
#mais sa corrélation avec ft_15 et ft_16 est égale à 1 

df_cat=df_cat.drop(['ft_17_categ','ft_9_categ','ft_7_categ','ft_15_categ','ft_16_categ','ft_8_categ','ft_10_categ','ft_11_categ','ft_12_categ','ft_13_categ','ft_14_categ',]
                    ,axis=1)
print(df_cat)

#Test de corrélation via' V de cramer' entre les variables catégorielles mises à jour

import association_metrics as am
# Convertir  les colonnes qui ont des str en  Category 
df_cat = df_cat.apply(
        lambda x: x.astype("category") if( x.dtype == "O") | (x.dtype == "int" ) else x)

# Initializer les objets de  CamresV en utilisant df_cramer
cramersv = am.CramersV(df_cat) 

#retournera une matrice par paires remplie du V de Cramer, où les colonnes et l’index sont les variables 
#catégorielles de df_cramer
corr_cramer=cramersv.fit()

#graphique de corrélation 
fig, ax = plt.subplots(figsize=(10,8))
sns.heatmap(corr_cramer, annot= True, ax= ax, cmap="viridis")
ax.set_title("Test de corrélation via' V de cramer' entre les variables catégorielles ")
plt.show()

#concateeeeee2énation des deux df num et cat
df_claim=pd.concat([df_num,df_cat],axis=1)
df_claim.columns

#test ANOVA
#création d'un multiIndex pour traiter toutes les combinaisons possibles pour le test ANOVA
index = pd.MultiIndex.from_product([['EXPO','superficief','ft_23_categ','ft_22_categ_c'],['ft_2_categ','ft_4_categ','ft_5_categ','ft_6_categ','ft_18_categ','ft_19_categ','ft_21_categ','ft_24_categ','target']],
                                   names=['cont', 'cat'])
tables = []
for cont_var,cat_var in index:
    model = ols('{} ~ {}'.format(cont_var,cat_var), data=df_claim).fit()
    anova_table = sm.stats.anova_lm(model, typ=2)

    tables.append(anova_table)

df_anova = pd.concat(tables, keys=index, axis=0)

#affichage filtrée de  df_anova
print(df_anova.filter(like = 'target', axis=0))

#pour les variables EXPO, superficief,les p_value(PR(>F)) sont inférieures à 5% donc on rejète H0 selon laquelle 
#ces variables n'influent pas sur target (la période assurée à Générali pendant l'année et la superficie  du batiment
# influent sur target)
#pour ft_23_categ et ft_22_categ_c, les p_value(PR(>F)) sont supérieure à 5% donc on accepte H0 
#selon laquelle target et les variables selectionnées sont indépendantes

#preprocessing level 2

print(df_claim.describe())

##séparation des features et de la target
feats = df_claim.drop('target', axis=1)
target=df_claim['target']

# Discrétiser ‘ft_22_categ_c’
feats['ft_2_categ'] = feats['ft_2_categ'].astype('float')
#on crée une nouvelle variable age_batiment
feats['age_batiment']=feats['ft_2_categ']-feats['ft_22_categ_c']
feats.drop(['ft_22_categ_c','ft_2_categ'],axis=1,inplace=True)

# Discrétiser ‘superficief’
feats.superficief = pd.cut(feats.superficief,[0,500,1002,2190,30745],
                            labels = ['petit','moyen','trés-grand','Géant'])

feats[['ft_5_categ','ft_6_categ']]=feats[['ft_5_categ','ft_6_categ']].replace(to_replace={'V':1,'O':1,'N':0})

#dummisation des feats
feats=pd.get_dummies(feats, prefix=['superficief','ft_18_categ'], 
                     columns=['superficief','ft_18_categ' ])

#exporter les données en local
feats.to_csv('feats_maj.csv', sep=';')
target.to_csv('target.csv', sep=';')


