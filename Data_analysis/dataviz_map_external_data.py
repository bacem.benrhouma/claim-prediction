#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 09:19:40 2021

@author: claudehaik
"""

import pandas as pd
import matplotlib.pyplot as plt 
import seaborn as sns
print('Construction du dataset des catastrophes naturelles pour une analyse par type et par lieu')
# Chargement du fichier externe des arrêtés de catastrophes naturelles
climat = pd.read_excel('Arretes_de_catastrophe_naturelles.xlsx', engine='openpyxl', index_col = None)
climat= climat.rename(columns ={'INSEE' : 'Insee'})
# Ajout de l'année à partir de la date de début de l'arrêté de catastrophe naturelle
climat['Annee_cata'] = pd.DatetimeIndex(climat['Date début']).year
# On choisit d'étudier les observations sur les 15 dernières années, 
# soit un ensemble de 40.000 observations
climat.Annee_cata.value_counts()
climat=climat[(climat['Annee_cata']>=2000) & (climat['Annee_cata']<=2016)]
# On choisit de ne garder que les 10 types de catastrophes les plus fréquents (soit 96% du total) et les regrouper en trois catégories
# Catégories de périls : actions du vent-pluie, actions de l'eau, actions du sol.

Meteo = ["Tempête","Inondations et chocs mécaniques liés à l'action des vagues",
       "Inondations, coulées de boue, glissements et chocs mécaniques liés à l'action des vagues",
       "Poids de la neige - chutes de neige"]

Sol = ["Mouvements de terrain différentiels consécutifs à la sécheresse et à la réhydratation des sols",
       "Mouvements de terrain consécutifs à la sécheresse","Mouvements de terrain"]

Eau = ["Inondations et coulées de boue", "Inondations, coulées de boue et mouvements de terrain",
       "Inondations, coulées de boue et glissements de terrain"]

# On remplace les 10 périls principaux par leur nom de catégorie - Eau, Sol ou Meteo
climat_reduit=climat.replace(to_replace = Eau, value = 'Eau')
climat_reduit=climat_reduit.replace(to_replace = Sol, value = 'Sol')
climat_reduit=climat_reduit.replace(to_replace = Meteo, value = 'Météo')

# On conserve un fichier des catastrophes naturelles réduit avec les seules lignes présentant un péril = Sol, Eau ou Meteo
climat_reduit = climat_reduit [(climat_reduit['Périls']=='Sol')|(climat_reduit['Périls']=='Météo')|(climat_reduit['Périls']=='Eau')]                                                                                                                                      
# vérification
print('Après compilation et réduction, sommes des trois catégories de catastrophes :')
climat_reduit.Périls.value_counts()
# Etape 1 de pré-processing : dummization du dataframe autour des 3 grands types de catastrophes naturelles
cat_dum = pd.get_dummies(climat_reduit['Périls'], prefix= 'Péril')
# suivi de l'ajout des colonnes dummizées au fichier de départ climat_reduit
climat_reduit = climat_reduit.join(cat_dum)
# Etape 2 du pré-processing : regroupement et somme par code INSEE 
# afin d'obtenir la somme des catastrophes naturelles par code INSEE et par type sur la période 1980-2016 
catastrophes = climat_reduit.groupby('Insee', as_index=False).sum()
catastrophes['Insee']=catastrophes['Insee'].astype(str)
# Création d'une variable 'Péril_total', somme de tous les arrêté de catastrophes par Insee
catastrophes['Péril_total']= catastrophes['Péril_Eau']+catastrophes['Péril_Météo']+catastrophes['Péril_Sol']
catastrophes.info()
# Etape 2 du pré-processing : regroupement et somme par code INSEE 
# afin d'obtenir la somme des catastrophes naturelles par code INSEE et par type sur la période 1980-2016 
catastrophes = climat_reduit.groupby('Insee', as_index=False).sum()
catastrophes['Insee']=catastrophes['Insee'].astype(str)
# Création d'une variable 'Péril_total', somme de tous les arrêté de catastrophes par Insee
catastrophes['Péril_total']= catastrophes['Péril_Eau']+catastrophes['Péril_Météo']+catastrophes['Péril_Sol']
catastrophes.info()
# Ajout des coordonnées géographiques longitude - latitude, département, région 
# trouvées sur Internet (corresponsdance Insee <-> coordonnées GPS)
geocoord=pd.read_csv('Codes geo_gps.csv', sep=';')
catastrophes_geo = catastrophes.merge(geocoord, on='Insee', how='inner')
# Sauvegarde de cet ensemble de 18.000 sites INSEE avec catastrophe naturelle
catastrophes_geo.to_csv('cata_geo.csv', index=False)
# A- ANALYSE GRAPHIQUE
# 1- Répartition des inondations par site
sns.countplot(catastrophes_geo.Péril_Eau)
plt.title('Distribution du nombre de sites INSEE touchés par des d\'inondations ')
plt.xlim(0,8)
plt.ylabel('quantité de sites INSEE')
plt.xlabel('quantité d\'inondations sur un même site INSEE')
plt.text(2,8000, '80% des sites sont touchés au moins une fois')
plt.show()
# 2- Répartition des catastrophes naturelles par régions
catastrophes_par_region = catastrophes_geo.groupby('Region', as_index=False).sum()
catastrophes_par_region_trie = catastrophes_par_region.sort_values(by = 'Péril_total', ascending=False)
catastrophes_par_region_trie.head(10).plot.bar(x='Region', y=['Péril_Eau','Péril_Météo', 'Péril_Sol'], stacked=True)
plt.title('Somme des catastrophes naturelles des 10 régions les plus touchées')
plt.ylabel('Nombre de catastrophes')
plt.xticks(rotation=90)
plt.show()
# B- ANALYSE CARTOGRAPHIQUE
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt

cata_geo=pd.read_csv('cata_geo.csv', sep=',')
# Carte de toutes les catastrophes naturelles entre 2000 et 2015 : il y en a trop !
fig = plt.figure(figsize=(15,15))
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
ax.set_extent([-5, 10, 42, 52])

ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.BORDERS, linestyle=':')
ax.add_feature(cfeature.RIVERS, linestyle='-')

plt.savefig('Carte_catastrophes_all.pdf')
plt.savefig('Carte_catastrophes_all.png')
ax.set_title('France - Catastrophes naturelles toutes catégories entre 2000 et 2016', fontsize=20);
sns.scatterplot(x='Longitude', y= 'Latitude', data=cata_geo, marker= 'o',linewidth=0 )
plt.show()
# Cartographie des catastrophes naturelles de type 'Tempêtes'
cata_geo_meteo=cata_geo[cata_geo['Péril_Météo']>=1]

fig = plt.figure(figsize=(15,15))
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
ax.set_extent([-5, 10, 42, 52])

ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.BORDERS, linestyle=':')
ax.add_feature(cfeature.RIVERS, linestyle='-')

plt.savefig('Carte_catastrophes_meteo.pdf')
plt.savefig('Carte_catastrophes_meteo.png')
ax.set_title('France - Catastrophes naturelles météo', fontsize=20);
plt.plot(cata_geo_meteo.Longitude, cata_geo_meteo.Latitude, 'c-', marker= 'o',linewidth=0)
plt.show()
# Pour cartographier les lieux les plus exposés à tout type de catastrophe naturelle,
# on crée un dataframe pour chaque type de catastrophe avec les sites les plus exposés.
top_eau = cata_geo[cata_geo['Péril_Eau']>=3]
top_eau_a= top_eau.assign(Categ='Inondation > 1 tous les 3 ans')

top_meteo = cata_geo[cata_geo['Péril_Météo']>=2]
top_meteo_a=top_meteo.assign(Categ= 'Tempête > 1 tous les 5 ans')

top_sol = cata_geo[cata_geo['Péril_Sol']>=4]
top_sol_a= top_sol.assign(Categ= 'Mouvmt de sol > 1 tous les 4 ans')
# Puis on assemblage les trois dataframes des sites les plus exposés part type
top_all= pd.concat([top_eau_a, top_sol_a, top_meteo_a])
# Cartographie des sites les plus exposés aux catastrophes naturelles de tout type 
fig = plt.figure(figsize=(15,15))
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
ax.set_extent([-5, 10, 42, 52])

ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.BORDERS, linestyle=':')
ax.add_feature(cfeature.RIVERS, linestyle='-')

ax.set_title('France - Sites les plus exposés aux catastrophes naturelles - par type', fontsize=25);
carte_plot=sns.scatterplot(x='Longitude', y= 'Latitude', data=top_all, marker= 'o',linewidth=0, hue='Categ')
plt.legend(title = "Types de catastrophes", fontsize = 'large', title_fontsize = 25, loc='upper left' )
fig = carte_plot.get_figure()
fig.savefig('Carte_catastrophes_all_hue.pdf')

plt.show()
