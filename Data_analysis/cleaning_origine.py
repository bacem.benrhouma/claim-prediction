#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 13:58:09 2021

@author: M.Benrhouma
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


#exploration des données
#importer les variables explicatives
X_train = pd.read_csv('X_train.csv')
X_train.head()
print( X_train)
print(X_train.info())

#suppression de la colonne Unnamed
X_train.drop(["Unnamed: 0"], axis = 1, inplace= True)

#data cleaning
X_train["EXPO"]=X_train["EXPO"].str.replace(',','.')
X_train["EXPO"]=X_train["EXPO"].apply(lambda x: float(x))

print(X_train.info())

##gestion des NaNs##

# On supprime les lignes Insee = NaN car elles sont inutilisables avec des données externes
# il ne s'agit que de 119 lignes (1% du dataset)
X_train.dropna(subset=['Insee'], how='all', inplace=True) 


#remplacer les NaNs de superficief par la mediane
#si on a des valeurs extremes , il est recommandé de remplacer les NaNs par la mediane
X_train['superficief'].fillna(X_train['superficief'].median(), inplace=True)

print(X_train.info())

#selection des variables catégorielles
X_train_cat=X_train.select_dtypes(include=['object','int'])

#vérification des modalités
for i in X_train_cat:
    print (X_train_cat[i].value_counts(normalize=True))
#la colonne ft_24_categ contient des '.' et '>=10'qu'on doit les remplacer par 0 et 10 

#remplacer les . et >=10 dans ft_24_categ 
X_train["ft_24_categ"]=X_train["ft_24_categ"].str.replace('.','0')
X_train["ft_24_categ"]=X_train["ft_24_categ"].str.replace('>=10','10')
X_train["ft_24_categ"]=X_train["ft_24_categ"].apply(lambda x: int(x))
print(X_train['ft_24_categ'].value_counts())

#remplacer les nan de ft_22_categ par knn.imputer
from sklearn.impute import KNNImputer
X=X_train['ft_22_categ'].values.reshape(-1,1)
imputer = KNNImputer(n_neighbors=26)
ft_22_categ_c=imputer.fit_transform(X)

#supprimer l'ancienne ft_24_categ et creer une nouvelle ft_22_categ_c sans nan
X_train.drop('ft_22_categ',axis=1,inplace=True)
X_train['ft_22_categ_c']=ft_22_categ_c

X_train.info()

#importer la variable cible
Y_train = pd.read_csv('y_train_saegPGl.csv')
print(Y_train.head())

#supprimer unnamed
Y_train.drop('Unnamed: 0',axis=1,inplace=True)

#jointure entre les deux jeux de données
claim = X_train.merge(right = Y_train, on = 'Identifiant', how = 'inner')
print(claim.head())
print(claim.info())

#exporter les données en local
claim.to_csv('claim_c.csv', sep=';')

