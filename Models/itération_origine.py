#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 12:39:25 2021

@author: M.Benrhouma
"""

#Import libraries:
import pandas as pd
import numpy as np
from numpy import mean
import matplotlib.pyplot as plt


from sklearn import ensemble
from sklearn import model_selection
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier

from sklearn.model_selection import train_test_split,cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer

from sklearn.utils.class_weight import compute_sample_weight
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold

#fonction pour le calcul de la metric 'gini_normalized'
def gini(actual, pred):
    assert (len(actual) == len(pred))
    all = np.asarray(np.c_[actual, pred, np.arange(len(actual))], dtype=np.float)
    all = all[np.lexsort((all[:, 2], -1 * all[:, 1]))]
    totalLosses = all[:, 0].sum()
    giniSum = all[:, 0].cumsum().sum() / totalLosses

    giniSum -= (len(actual) + 1) / 2.
    return giniSum / len(actual)


def gini_normalized(actual, pred):
    return gini(actual, pred) / gini(actual, actual)

score=make_scorer(gini_normalized,greater_is_better = True)

#import des données
target=pd.read_csv('target.csv', sep=';',index_col='Identifiant')
feats=pd.read_csv('feats_maj.csv', sep=';',index_col='Identifiant')

target['target'].value_counts(normalize=True)
#le modéle est déséquilibré

### Dataframes
X_feats = pd.DataFrame(feats)
Y_target = np.ravel(target)

X_train, X_test, y_train, y_test = train_test_split(X_feats, Y_target, test_size=0.2,random_state=42)

#on  prépare la  configuration pour la méthode de  cross validation 
seed = 42
# prepare models
models = []
models.append(('GB', GradientBoostingClassifier()))
models.append(('RF', RandomForestClassifier()))
models.append(('XGB', XGBClassifier()))
models.append(('LR', LogisticRegression()))
models.append(('SVM', SVC()))

# evaluer  chaque modèle 
results = []
names = []
scoring = score
x=[1,2,3,4,5]
for name, model in models:
    kfold = model_selection.KFold(n_splits=10, random_state=seed)
    cv_results = model_selection.cross_val_score(model, X_train, y_train,
                                                 cv=kfold, scoring=score)
    results.append(cv_results)
    names.append(name)
    msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
    print(msg)
# boxplot algorithm comparison
fig = plt.figure()
plt.boxplot(results)
plt.xticks(x, names)
plt.title('boxplot algorithm comparison')
plt.show()

#À partir de ces résultats, il semblerait que le GradientBoostingClassifier, le random forest et le XGboost méritent
#peut-être d’être étudiées plus pour notre  problème de classification .

#GradientBoostingClassifier parametres par defaut (sans corriger le desequilibre )
model = GradientBoostingClassifier()
model.fit(X_train, y_train)
# feature importance
print(model.feature_importances_)
importance=model.feature_importances_
#plot Feature Importances
feat_imp = pd.Series(model.feature_importances_, feats.columns).sort_values(ascending=False)
plt.figure(figsize=(12,12))
feat_imp.plot(kind='bar')
plt.ylabel('Feature Importance Score ')
plt.title('Feature Importance GB imbalanced')
plt.show()

y_test_predictions=model.predict(X_test)
print("gini_normalized test sans optimiser les paramètres : %.2g" % gini_normalized(y_test, y_test_predictions))

y_train_predictions=model.predict(X_train)
print("gini_normalized  train sans optimiser les paramètres : %.2g" % gini_normalized(y_train, y_train_predictions))





#compute_sample_weight  permet d'attribuer des poids à chaque observation et non aux classes quand il s'agit 
#d'un modèle déséquilibré.

model_balanced = GradientBoostingClassifier()
model_balanced.fit(X_train, y_train, compute_sample_weight(class_weight='balanced', y=(y_train)))
# feature importance
print(model_balanced.feature_importances_)
importance=model_balanced.feature_importances_
#plot Feature Importances
feat_imp = pd.Series(model_balanced.feature_importances_, feats.columns).sort_values(ascending=False)
plt.figure(figsize=(12,12))
feat_imp.plot(kind='bar', title='Feature Importances GB balanced')
plt.ylabel('Feature Importance Score')
y_test_predictions=model_balanced.predict(X_test)
plt.show()

print("gini_normalized test sans optimiser les paramètres : %.2g" % gini_normalized(y_test, y_test_predictions))

y_train_predictions=model_balanced.predict(X_train)
print("gini_normalized  train sans optimiser les paramètres : %.2g" % gini_normalized(y_train, y_train_predictions))
#on a pu améliorer le score 

#les variables superficief_géant ,EXPO,age du batiment ,ft_21_categ et superficief_trés_grand sont les plus importantes selon
#le modéle Gradient boosting classifier equilibré

#on optimise les n_estimators
param_test1 = {'n_estimators':range(20,301,20)}

gsearch1 = GridSearchCV(estimator = GradientBoostingClassifier(), 
param_grid = param_test1, scoring=score,n_jobs=4,iid=False, cv=5)

gsearch1.fit(X_train,y_train,compute_sample_weight(class_weight='balanced', y=(y_train)))
y_test_pred=gsearch1.predict(X_test)


print ("\nModel Report 1")
print("gini_normalized test model 1: %.4g" % gini_normalized(y_test, y_test_pred))
y_train_predictions=gsearch1.predict(X_train)
print("gini_normalized  train model 1: %.4g" % gini_normalized(y_train, y_train_predictions))
print('\n best  params:')
print(gsearch1.best_params_)

#graphique pour comparer l'evolution du gini score en fonction  des n_estimators
n_estimators = [1, 2, 4, 8, 16, 32, 64, 100, 200,300]
train_results = []
test_results = []
for estimator in n_estimators:
    GB = GradientBoostingClassifier(n_estimators=estimator)
    GB.fit(X_train, y_train,compute_sample_weight(class_weight='balanced', y=(y_train)))
    train_pred = GB.predict(X_train)
    score_gini_train=gini_normalized(y_train, train_pred)
    train_results.append(score_gini_train)
    y_pred = GB.predict(X_test)
    score_gini_test = gini_normalized(y_test, y_pred)
    test_results.append(score_gini_test)
from matplotlib.legend_handler import HandlerLine2D
line1, = plt.plot(n_estimators, train_results, 'b', label='Train gini_normalized')
line2, = plt.plot(n_estimators, test_results, 'r', label='Test gini_normalized')
plt.legend(handler_map={line1: HandlerLine2D(numpoints=2)})
plt.ylabel('gini_normalized')
plt.xlabel('n_estimators')
plt.title('Evolution du gini score en fonction  des n_estimators pour GB model ')
plt.show()


#augmenter le nombre d'estimateurs engendre un overfitting d'aprés le graphique n_estimateurs = 150 est optimal
#l'ecart entre le score du train et de test n'est pas assez grand 

#GradientBoostingClassifier pour n_estimators=150
GB_optimise = GradientBoostingClassifier(n_estimators=150)
GB_optimise.fit(X_train, y_train, compute_sample_weight(class_weight='balanced', y=(y_train)))

y_test_predictions_op=GB_optimise.predict(X_test)
print("gini_normalized test avec n_estimators =150 : %.2g" % gini_normalized(y_test, y_test_predictions_op))

y_train_predictions_op=GB_optimise.predict(X_train)
print("gini_normalized  train n_estimators=150 : %.2g" % gini_normalized(y_train, y_train_predictions_op))

###learning rates
learning_rates = [1, 0.5, 0.25, 0.1, 0.05, 0.01]
train_results = []
test_results = []
for eta in learning_rates:
    model = GradientBoostingClassifier(learning_rate=eta)
    model.fit(X_train, y_train,compute_sample_weight(class_weight='balanced', y=(y_train)))
    train_pred = model.predict(X_train)
    score_gini_train=gini_normalized(y_train, train_pred)
    train_results.append(score_gini_train)
    y_pred = GB.predict(X_test)
    score_gini_test = gini_normalized(y_test, y_pred)
    test_results.append(score_gini_test)
from matplotlib.legend_handler import HandlerLine2D
line1, = plt.plot(learning_rates, train_results, 'b', label='Train gini_normalized')
line2, = plt.plot(learning_rates, test_results, 'r', label='Test gini_normalized')
plt.legend(handler_map={line1: HandlerLine2D(numpoints=2)})
plt.ylabel('gini_normalized')
plt.xlabel('learning_rates')
plt.title('Evolution du gini score en fonction  des learning_rates : GB')
plt.show()

#on remarque que l'utilisation d'un taux d'apprentissage élevé entraîne un surapprentissage.
#Pour ces données, un taux d'apprentissage de 0.1 est optimal.


#GradientBoostingClassifier optimisé
GB_optimised = GradientBoostingClassifier(n_estimators=150,learning_rate=0.1)
GB_optimised.fit(X_train, y_train, compute_sample_weight(class_weight='balanced', y=(y_train)))
# feature importance
print(GB_optimised.feature_importances_)
importance=GB_optimised.feature_importances_
#plot Feature Importances
feat_imp = pd.Series(GB_optimised.feature_importances_, feats.columns).sort_values(ascending=False)
plt.figure(figsize=(12,12))
feat_imp.plot(kind='bar', title='Feature Importances pour GB optimisé ')
plt.ylabel('Feature Importance Score')
plt.show()
y_test_predictions_op=GB_optimised.predict(X_test)
print("gini_normalized test  : %.2g" % gini_normalized(y_test, y_test_predictions_op))

y_train_predictions_op=GB_optimised.predict(X_train)
print("gini_normalized train : %.2g" % gini_normalized(y_train, y_train_predictions_op))

#les scores du test et du train restent inchangé donc on passe à l'optimisation d'un autre modèle

###random forest model
# #random forest model paramètres par default
rf = RandomForestClassifier(bootstrap=True, class_weight='balanced', criterion='gini',
                             max_depth=None, max_features='auto', max_leaf_nodes=None,
                              min_samples_leaf=1,
                             min_samples_split=2, min_weight_fraction_leaf=0.0,
                             n_estimators=10, n_jobs=1, oob_score=False, random_state=None,
                             verbose=0, warm_start=False)
rf.fit(X_train, y_train)
# feature importance
print(rf.feature_importances_)
importance=rf.feature_importances_
#plot Feature Importances
feat_imp = pd.Series(rf.feature_importances_, feats.columns).sort_values(ascending=False)
plt.figure(figsize=(12,12))
feat_imp.plot(kind='bar', title='Feature Importances rf  par default')
plt.ylabel('Feature Importance Score')
plt.show()

y_test_predictions=rf.predict(X_test)
print("gini_normalized test random forest model : %.2g" % gini_normalized(y_test, y_test_predictions))

y_train_predictions=rf.predict(X_train)
print("gini_normalized train random forest model : %.2g" % gini_normalized(y_train, y_train_predictions))

#les variables superficief_géant , EXPO demeurent les plus importantes selon le modéle random forest par contre on 
#on remarque que ft_23_categ et ft_2_categ_2013 apparaissent dans le top 4 feature importance

#un grand ecart entre  le gini_normalized test pour le modèle random forest  (0.13)
# et le gini_normalized train random forest  : 0.7
#on essaye de corriger cet overfitting à travers l'optimisation  des n_estimators
n_estimators = [1, 2, 4, 8, 16, 32, 64, 100, 200,300]
train_results = []
test_results = []
for estimator in n_estimators:
    rf_op = RandomForestClassifier(n_estimators=estimator, n_jobs=-1,class_weight='balanced')
    rf_op.fit(X_train, y_train)
    train_pred = rf_op.predict(X_train)
    score_gini_train=gini_normalized(y_train, train_pred)
    train_results.append(score_gini_train)
    y_pred = rf_op.predict(X_test)
    score_gini_test = gini_normalized(y_test, y_pred)
    test_results.append(score_gini_test)
from matplotlib.legend_handler import HandlerLine2D
line1, = plt.plot(n_estimators, train_results, 'b', label='Train gini_normalized')
line2, = plt.plot(n_estimators, test_results, 'r', label='Test gini_normalized')
plt.legend(handler_map={line1: HandlerLine2D(numpoints=2)})
plt.ylabel('gini_normalized')
plt.xlabel('n_estimators')
plt.title('Evolution du gini score en fonction  des n_estimators pour le model random forest')
plt.show()

# il y a un ecart important entre le score de test et le score d'entrainement

#on optimise max_depths
max_depths = np.linspace(1, 32, 32, endpoint=True)
train_results = []
test_results = []
for max_depth in max_depths:
    rf = RandomForestClassifier(max_depth=max_depth, n_jobs=-1,class_weight='balanced')
    rf.fit(X_train, y_train)
    train_pred = rf.predict(X_train)
    score_gini_train=gini_normalized(y_train, train_pred)
    train_results.append(score_gini_train)
    y_pred = rf_op.predict(X_test)
    score_gini_test = gini_normalized(y_test, y_pred)
    test_results.append(score_gini_test)
from matplotlib.legend_handler import HandlerLine2D
line1, = plt.plot(max_depths, train_results, 'b', label='Train gini_normalized')
line2, = plt.plot(max_depths, test_results, 'r', label='Test gini_normalized')
plt.legend(handler_map={line1: HandlerLine2D(numpoints=2)})
plt.ylabel('gini_normalized')
plt.xlabel('max_depths')
plt.title('gini score vs depth optimisation random forest model')
plt.show()


# il y a un ecart important entre le score de test et le score d'entrainement
#pour notre cas le random forest n'est pas le meilleur modéle qu'on pourra le valider on passe à XGBClassifier
##XGBClassifier


# on corrige le desequilibre du jeu de donnée a travers l'argument 'scale_pos_weight' (99 est recommandé selon les études
#faites sur ce sujet
model = XGBClassifier(scale_pos_weight=99)
# define evaluation procedure
cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
# evaluate model
scores = cross_val_score(model, X_train, y_train, scoring=score, cv=cv, n_jobs=-1)
# summarize performance
print('moyenne gini: %.4f' % mean(scores))

#XGBClassifier
model = XGBClassifier(scale_pos_weight=99)
model.fit(X_train, y_train)
# make predictions for test and train data 
y_pred_test = model.predict(X_test)
y_pred_train = model.predict(X_train)


print("gini test : %.4f" % gini_normalized(y_test, y_pred_test))
print("gini train : %.4f" % gini_normalized(y_train, y_pred_train))


#XGBoost hyper-parameter tuning
def hyperParameterTuning(X_train, y_train):
    param_tuning = {
        'learning_rate': [0.01, 0.1],
        'max_depth': [3, 5, 7, 10],
        'min_child_weight': [1, 3, 5],
        'scale_pos_weight' :[99,1]
        }

    xgb_model = XGBClassifier()

    gsearch = GridSearchCV(estimator = xgb_model,
                           param_grid = param_tuning,                        
                           scoring = score,
                           cv = 5,
                           n_jobs = -1,
                           verbose = 1)

    gsearch.fit(X_train,y_train)

    return gsearch.best_params_

#XGBoost hyper-parameter tuning 2
def hyperParameterTuning2(X_train, y_train):
    param_tuning = {
        'subsample': [0.5, 0.7],
        'colsample_bytree': [0.5, 0.7],
        'n_estimators' : [100, 200, 500],
        'objective': ['binary:logistic']
        }

    xgb_model = XGBClassifier(learning_rate=0.1,max_depth=10,min_child_weight=1,scale_pos_weight=1)

    gsearch = GridSearchCV(estimator = xgb_model,
                           param_grid = param_tuning,                        
                           scoring = score,
                           cv = 5,
                           n_jobs = -1,
                           verbose = 1)

    gsearch.fit(X_train,y_train)

    return gsearch.best_params_

hyperParameterTuning(X_train, y_train)
hyperParameterTuning2(X_train, y_train)

#best fit
xgb_model = XGBClassifier(
        objective = 'binary:logistic',
        colsample_bytree = 0.5,
        learning_rate = 0.05,
        max_depth =6 ,
        min_child_weight = 1,
        n_estimators = 1000,
        subsample = 0.7,
        scale_pos_weight=99)

xgb_model.fit(X_train,y_train)

y_pred_xgb = xgb_model.predict(X_test)
y_pred_xgb_t = xgb_model.predict(X_train)

gini_score_train = gini_normalized(y_train, y_pred_xgb_t)


gini_score = gini_normalized(y_test, y_pred_xgb)

print("gini_normalized test XGboost best fit : ", gini_score)
print("gini_normalized train XGboost best fit: ", gini_score_train)

#plot Feature Importances
feat_imp = pd.Series(xgb_model.feature_importances_, feats.columns).sort_values(ascending=False)
plt.figure(figsize=(12,12))
feat_imp.plot(kind='bar', title='Feature Importances XGboost best fit ')
plt.ylabel('Feature Importance Score')
plt.show()
#il y a toujours un ecart entre le test et le train donc on grade le gradient bossting  classifier comme  un meilleur
#modèle pour notre cas





